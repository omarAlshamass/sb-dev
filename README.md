# Sb Dev



## Getting started

To setup this project just you have to:<br />

1- Please, clone this project in any device that has docker `git clone https://gitlab.com/omarAlshamass/sb-dev.git safeboda`. <br />
2- Navigate to the `containers` folder by executing the command `cd ./safeboda/docker/environments/development/containers/` <br />
3- Run this command `bash entry-point.sh` to initialize the four containers `promo-code-backend`, `2 DB containers` and `Radis`.<br />
4- Please, just wait until the 14 test cases in bash script are done and then you can access and test the code.<br />
Now you can open your browser and access the app under `localhost:8888` URL.<br />

**Note:**\
1- To test the endpoints, you can find them in `doc\APIs\promo-code` folder in `promo-code-backend` repository which will be pull automatically via the bash script, 
so the full path of the `doc` folder should be `safeboda/projects/promo-code-backend/doc/APIs/promo-code` <br />
2- I have not added all the test cases that supposed to be for this logic since I have not found enough time to cover all the parts of the code, so I added the most importants one only. <br />
3- I docorized the containers for development purpose only, for production We have to do it different way, like: <br /> 
    A- Removing the testing DB's container after finishing the CI/CD, <br />
    B- Making the environment variables inside the host, not in a file. <br />
    C- Optimizing the code. <br />
    D- Securing the connection between Radis and the rest containers. <br />
    E- Doing some caching for the GET APIs that we have, etc. <br />
 
That is all. <br />
Thanks. <br />
