#!/bin/bash
tput setaf 1; echo "1- The command of initialization of the containers is going to perform now."

cd ../../../../
mkdir -p projects/promo-code-backend
cd projects/promo-code-backend
git clone https://gitlab.com/omarAlshamass/sb-pc.git .
sleep 5

cd ../../docker/environments/development/containers
docker-compose up --build -d
tput setaf 1; echo "2- We are going to wait for 20 seconds to give the postgres container some time to startup, in case the upcoming commands failed please, run them manually";


sleep 20
docker exec -it promo-code-backend composer install
docker exec -it promo-code-backend php artisan migrate
docker exec -it promo-code-backend php artisan db:seed
docker exec -it promo-code-backend php artisan migrate --env=testing
docker exec -it promo-code-backend php artisan test --env=testing
docker exec -it promo-code-backend php artisan queue:work